﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

using HospitalProject.Models;

namespace HospitalProject.Data
{
    public class HospitalDbContext : IdentityDbContext<ApplicationUser>
    {
        public HospitalDbContext(DbContextOptions<HospitalDbContext> options)
        : base(options)
        {

        }

        public DbSet<Admin> Admins { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Department> Departments { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Faq> Faqs { get; set; }
        public DbSet<JobPosting> JobPostings { get; set; }
        public DbSet<JobQualification> JobQualifications { get; set; }
        public DbSet<Qualification> Qualifications { get; set; }
        public DbSet<Booking> Bookings { get; set; }
        public DbSet<Donation> Donations { get; set; }
        public DbSet<Page> Pages { get; set; }
        public DbSet<Link> Links { get; set; }
        public DbSet<Notification> Notifications { get; set; }
        public DbSet<FindSpecialist> FindSpecialists { get; set; }
        public DbSet<ReachUs> Reach_Us { get; set; }
        public DbSet<VaccinationAlerts> Vaccination_Alerts { get; set; }
        public DbSet<emergencyWaitingtime> emergency_Waitingtime { get; set; }
        public DbSet<Blogs> BLog { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<JobQualification>()
                .HasKey(jq => new { jq.JobPostingID, jq.QualificationID });

            modelBuilder.Entity<JobQualification>()
                .HasOne(jq => jq.JobPosting)
                .WithMany(jp => jp.JobQualifications)
                .HasForeignKey(jq => jq.JobPostingID);

            modelBuilder.Entity<JobQualification>()
                .HasOne(jq => jq.Qualification)
                .WithMany(q => q.JobQualifications)
                .HasForeignKey(jq => jq.QualificationID);

            modelBuilder.Entity<JobPosting>()
                .HasOne(jp => jp.Department)
                .WithMany(d => d.JobPostings)
                .HasForeignKey(jp => jp.DepartmentID);

            modelBuilder.Entity<JobPosting>() //this is talking about jobpostings
                .HasOne(jp => jp.Admin) //job posting has one admin, but now we are talking about admins
                .WithMany(a => a.JobPostings)//admin has many jobpostings, but now we are talking about jobpostings again
                .HasForeignKey(jp => jp.AdminID);//a job posting has an admin id

            modelBuilder.Entity<Department>()
                .HasOne(d => d.Admin)
                .WithMany(a => a.Departments)
                .HasForeignKey(d => d.AdminID);

            modelBuilder.Entity<Page>()
                .HasOne(d => d.Admin)
                .WithMany(a => a.Pages)
                .HasForeignKey(d => d.AdminID);

            modelBuilder.Entity<Faq>()
                .HasOne(f => f.Category)
                .WithMany(c => c.Faqs)
                .HasForeignKey(f => f.CategoryID);

            modelBuilder.Entity<Faq>()
                .HasOne(f => f.Admin)
                .WithMany(a => a.Faqs)
                .HasForeignKey(f => f.AdminID);

            modelBuilder.Entity<Booking>()
                .HasOne(b => b.Department)
                .WithMany(d => d.Bookings)
                .HasForeignKey(b => b.DepartmentID);

            // https://github.com/aspnet/EntityFrameworkCore/issues/3815
            modelBuilder.Entity<Faq>()
                .HasOne(f => f.Category)
                .WithMany(c => c.Faqs)
                .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<JobPosting>()
                .HasOne(jp => jp.Department)
                .WithMany(d => d.JobPostings)
                .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<JobQualification>()
                .HasOne(jq => jq.Qualification)
                .WithMany(q => q.JobQualifications)
                .Metadata.DeleteBehavior = DeleteBehavior.Restrict;

            modelBuilder.Entity<Category>()
                .HasOne(c => c.Admin)
                .WithMany(a => a.Categories)
                .HasForeignKey(c => c.AdminID);

            modelBuilder.Entity<Event>()
                .HasOne(e => e.Admin)
                .WithMany(a => a.Events)
                .HasForeignKey(e => e.AdminID);

            modelBuilder.Entity<Admin>()
                .HasOne(a => a.User)
                .WithOne(u => u.Admin)
                .HasForeignKey<ApplicationUser>(u => u.AdminID);

            modelBuilder.Entity<ReachUs>()
                .HasOne(d => d.Department)
                .WithMany(r => r.reachUs)
                .HasForeignKey(d => d.DepartmentID);

            modelBuilder.Entity<VaccinationAlerts>()
                .HasOne(Va => Va.Admin)
                .WithMany(u => u.VaccinationAlerts)
                .HasForeignKey(u => u.AdminID);

            modelBuilder.Entity<emergencyWaitingtime>()
               .HasOne(Va => Va.Admin)
               .WithMany(u => u.emergencyWaitingtimes)
               .HasForeignKey(u => u.AdminID);

            modelBuilder.Entity<Blogs>()
               .HasOne(Va => Va.Admin)
               .WithMany(u => u.Blogs)
               .HasForeignKey(u => u.AdminID);

            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Admin>().ToTable("Admins");
            modelBuilder.Entity<Department>().ToTable("Departments");
            modelBuilder.Entity<JobPosting>().ToTable("JobPostings");
            modelBuilder.Entity<Qualification>().ToTable("Qualifications");
            modelBuilder.Entity<JobQualification>().ToTable("JobQualifications");
            modelBuilder.Entity<Category>().ToTable("Categories");
            modelBuilder.Entity<Faq>().ToTable("Faqs");
            modelBuilder.Entity<Event>().ToTable("Events");
            modelBuilder.Entity<VaccinationAlerts>().ToTable("Vaccination_Alerts");
            modelBuilder.Entity<emergencyWaitingtime>().ToTable("emergency_Watiningtime");
            modelBuilder.Entity<Blogs>().ToTable("Blog");

        }
    }
}
