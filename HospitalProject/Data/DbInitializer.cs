﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalProject.Data
{
    public static class DbInitializer
    {
        // From Christine's Example
        public static void Initialize(HospitalDbContext context)
        {
            context.Database.EnsureCreated();

            return;
        }
    }
}
