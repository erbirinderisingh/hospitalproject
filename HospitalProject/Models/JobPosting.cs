﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class JobPosting
    {
        [Key]
        public int JobPostingID { get; set; }

        [Required, StringLength(255), Display(Name = "Title")]
        public string Title { get; set; }

        [Required, StringLength(255), Display(Name = "Description")]
        public string Description { get; set; }

        [Required, Display(Name = "Date Posted")]
        public DateTime DatePosted{ get; set; }

        [Required, Display(Name = "Deadline")]
        public DateTime Deadline { get; set; }

        [Required, Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [InverseProperty("JobPosting")]
        public List<JobQualification> JobQualifications { get; set; }

        [ForeignKey("DeparmentID")]
        public int DepartmentID { get; set; }

        public virtual Department Department { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
