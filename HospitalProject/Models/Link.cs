﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HospitalProject.Models
{
    public class Link
    {
        [Key]
        public int LinkID { get; set; }

        [Required, StringLength(255), Display(Name = "Link Text")]
        public string LinkText { get; set; }

        [Required, StringLength(255), Display(Name = "Link Type")]
        public string LinkType { get; set; }

        [Required, StringLength(255), Display(Name = "External URL")]
        public string LinkExternalURL { get; set; }

        [Required, Display(Name = "Created Date")]
        public DateTime LinkCreatedDate { get; set; }

        [Required, Display(Name = "Modified Date")]
        public DateTime LinkUpdatedDate { get; set; }

        [Required, Display(Name = "Link Status")]
        public bool IsPublished { get; set; }

        [Required, Display(Name = "Link Deleted?")]
        public bool IsDeleted { get; set; }

        public virtual Page page { get; set; }
    }
}
