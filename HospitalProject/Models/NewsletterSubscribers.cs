﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class NewsletterSubscribers
    {
        [Key]
        public int id { get; set; }

        [Required, StringLength(255), Display(Name = "Name")]
        public string name { get; set; }

        [Required, StringLength(255), Display(Name = "email")]
        public string email { get; set; }

        [Required, StringLength(255), Display(Name = "frequency(In days")]
        public int frequency { get; set; }

    }
}
