﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class ReachUs
    {
        [Key]
        public int ReachUsID { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string FName { get; set; }

        [Required, StringLength(50), Display(Name = "Last Name")]
        public string LName { get; set; }

        [Required, Display(Name = "Phone Number")]
        public int Phone { get; set; }

        [Required, StringLength(100), Display(Name ="Email")]
        public string Email { get; set; }

        [StringLength(500), Display(Name ="Comment")]
        public string Comment { get; set; }

        // foreign key of DepartmentID
        [ForeignKey("DepartmentID")]
        public int DepartmentID { get; set; }

        public virtual Department Department { get; set; }
    }
}
