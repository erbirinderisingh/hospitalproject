﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Qualification
    {
        [Key]
        public int QualificationID { get; set; }

        [Required, StringLength(255), Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }

        [InverseProperty("Qualification")]
        public List<JobQualification> JobQualifications { get; set; }
    }
}
