﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Blogs
    {
        [Key]

        public int Blogid { get; set; }

        [Required, StringLength(500), Display(Name = "Title")]
        public string Title { get; set; }

        [Required, StringLength(500), Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }
}
