﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class VaccinationAlerts
    {
        [Key]

        public int VaccinationAlertsid { get; set; }

        [Required, StringLength(255), Display(Name = "Name")]
        public string Name { get; set; }

        [Required,  Display(Name = "Address")]
        public string Address { get; set; }

        [Required, Display(Name = "Age")]
        public string Age { get; set; }

        [Required, Display(Name = "VaccinationNumber")]
        public string VaccinationNumber { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }


        //[InverseProperty("VaccinationAlerts")]
        //public List<Department> Departments { get; set; }
    }

}
    

