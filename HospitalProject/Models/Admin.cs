﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Admin
    {
        [Key]
        public int AdminID { get; set; }

        [Required, StringLength(255), Display(Name = "First Name")]
        public string FirstName { get; set; }

        [Required, StringLength(255), Display(Name = "Last Name")]
        public string LastName { get; set; }

        [ForeignKey("User ID")]
        public string UserID { get; set; }

        public virtual ApplicationUser User { get; set; }

        [InverseProperty("Admin")]
        public List<JobPosting> JobPostings{ get; set; }

        [InverseProperty("Admin")]
        public List<Event> Events { get; set; }

        [InverseProperty("Admin")]
        public List<Faq> Faqs { get; set; }

        [InverseProperty("Admin")]
        public List<Category> Categories { get; set; }

        [InverseProperty("Admin")]
        public List<Department> Departments { get; set; }

        [InverseProperty("Admin")]
        public List<Page> Pages { get; set; }

        [InverseProperty("Admin")]
        public List<VaccinationAlerts> VaccinationAlerts{ get; set; }

        [InverseProperty("Admin")]
        public List<Blogs> Blogs{ get; set; }

        [InverseProperty("Admin")]
        public List<emergencyWaitingtime> emergencyWaitingtimes { get; set; }
    }
}
