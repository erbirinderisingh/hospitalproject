﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Notification
    {

        [Key]
        public int NotificationID { get; set; }

        [Required, StringLength(255), Display(Name = "Notification Title")]
        public string NotificationTitle { get; set; }

        [Required, StringLength(2000), Display(Name = "Notification Text")]
        public string NotificationText { get; set; }

        [Required, Display(Name = "Notification Start Date")]
        public DateTime NotificationStartDate { get; set; }

        [Required, Display(Name = "Notification End Date")]
        public DateTime NotificationEndDate { get; set; }

        [Required, Display(Name = "Notification Created Date")]
        public DateTime NotificationCreatedDate { get; set; }

        [Required, Display(Name = "Notification Updated Date")]
        public DateTime NotificationUpdatedDate { get; set; }

        [Required, Display(Name = "Notification Status")]
        public bool IsPublished { get; set; }

        [Required, Display(Name = "Notification Deleted")]
        public bool IsDeleted { get; set; }

        public virtual ApplicationUser PageAuthor { get; set; }
    }
}
