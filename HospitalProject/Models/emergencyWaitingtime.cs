﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HospitalProject.Models
{
    public class emergencyWaitingtime
    {
        [Key]

        public int PatientID { get; set; }

        [Required, Display(Name = "Name")]
        public string Name { get; set; }

        [Required, Display(Name = "Address")]
        public string Address { get; set; }

        [Required, Display(Name = "Age")]
        public string Age { get; set; }

        [Required, Display(Name = "ArrivalTime")]
        public string ArrivalTime { get; set; }

        [Required, Display(Name = "DoctorsTime")]
        public string DoctorsTime { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }

        public virtual Admin Admin { get; set; }
    }

}
