﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Newsletter
    {
        [Key]
        public int id { get; set; }

        [Required, StringLength(255), Display(Name = "Post Title")]
        public string name { get; set; }

        [Required, StringLength(255), Display(Name = "post Details")]
        public string post_details { get; set; }

        [Required, StringLength(255), Display(Name = "Date Created")]
        public DateTime date_created { get; set; }

    }
}
