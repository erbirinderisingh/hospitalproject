﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class JobQualification
    {
        [Key]
        public int JobQualificationID { get; set; }

        [ForeignKey("JobPostingID")]
        public int JobPostingID { get; set; }

        public virtual JobPosting JobPosting { get; set; }

        [ForeignKey("QualificationID")]
        public int QualificationID { get; set; }

        public virtual Qualification Qualification { get; set; }
    }
}
