﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Visitor
    {
        [Key]
        public int id { get; set; }

        [Required, StringLength(255), Display(Name = "Visitor Name")]
        public string name { get; set; }

        [Required, StringLength(255), Display(Name = "Visitor Phone Number")]
        public int phone { get; set; }

        [Required, StringLength(255), Display(Name = "Visitor Email")]
        public string email { get; set; }
  


    }
}
