﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalProject.Models.ViewModels
{
    public class FaqEdit
    {
        public FaqEdit()
        {

        }

        public virtual Faq Faq { get; set; }

        public IEnumerable<Category> Categories { get; set; }
    }
}
