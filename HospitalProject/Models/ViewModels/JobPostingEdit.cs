﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HospitalProject.Models.ViewModels
{
    public class JobPostingEdit
    {
        public JobPostingEdit()
        {

        }

        public virtual JobPosting JobPosting { get; set; }

        public IEnumerable<Department> Departments { get; set; }

        public IEnumerable<Qualification> Qualifications { get; set; }
    }
}
