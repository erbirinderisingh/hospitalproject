﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class Department
    {
        [Key]
        public int DepartmentID { get; set; }

        [Required, StringLength(255), Display(Name = "Name")]
        public string Name { get; set; }

        [Required, StringLength(255), Display(Name = "Description")]
        public string Description { get; set; }

        [ForeignKey("AdminID")]
        public int AdminID { get; set; }


        //[ForeignKey("VaccinationAlertsid")]
        //public int VaccinationAlertsid { get; set; }

        public virtual Admin Admin { get; set; }

        [InverseProperty("Department")]
        public List<JobPosting> JobPostings { get; set; }

        [InverseProperty("Department")]
        public List<Booking> Bookings { get; set; }

        [InverseProperty("Department")]
        public List<ReachUs> reachUs { get; set; }
    }
}
