﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HospitalProject.Models
{
    public class FindSpecialist
    {
        [Key]
        public int SpecialistID { get; set; }

        [Required, StringLength(50), Display(Name = "First Name")]
        public string SpecialistFName { get; set; }

        [Required, StringLength(50), Display(Name = "Last Name")]
        public string SpecialistLName { get; set; }

        [Required, Display(Name ="Phone Number")]
        public int Phone { get; set; }

        [Required, StringLength(10), Display(Name ="Gender") ]
        public string Gender { get; set; }

        [Required, StringLength(30), Display(Name = "Speciality")]
        public string Speciality { get; set; }

        [Required, Display(Name ="Available Day")]
        public DayOfWeek AvailableDay { get; set; }

        [Required, Display(Name ="Available Time")]
        public DateTime AvailableTime { get; set; }


    }
}
