﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using System.Diagnostics;

namespace HospitalProject.Controllers
{
    // Admin Controller and respective views references Christine's example for Authors
    public class AdminController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public AdminController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.Admins.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Admins.ToListAsync());
            }
        }

        public async Task<ActionResult> Details(int? id)
        {
            var user = await GetCurrentUserAsync();
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            Admin located_admin = await db.Admins.SingleOrDefaultAsync(a => a.AdminID == id);
            if (located_admin == null)
            {
                return NotFound();
            }
            if (user != null)
            {
                if (user.Admin == located_admin)
                {
                    ViewData["UserIsAdmin"] = "True"; //User because we found a match
                }
                else
                {
                    ViewData["UserIsAdmin"] = "False"; //Not a user because it's a mismatch
                }
            }
            else
            {
                ViewData["UserIsAdmin"] = "False"; //Not a user because they have no account
            }


            return View(located_admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("FirstName, LastName")] Admin admin)
        {
            if (ModelState.IsValid)
            {
                db.Admins.Add(admin);
                db.SaveChanges();
                var res = await MapUserToAdmin(admin);

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            Admin admin = db.Admins.Find(id);
            if (admin == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != id)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(admin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("AdminID,FirstName,LastName")] Admin admin)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID != admin.AdminID)
            {
                return Forbid();//forbid because they have the wrong account
            }

            var webRoot = _env.WebRootPath;

            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(admin);
        }

        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }

            Admin located_admin = await db.Admins.SingleOrDefaultAsync(a => a.AdminID == id);
            if (located_admin == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != id)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(located_admin);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            Admin author = await db.Admins.FindAsync(id);
            var user = await GetCurrentUserAsync();
            if (user.AdminID != id)
            {
                return Forbid();
            }
            await UnmapUserFromAdmin(id);
            db.Admins.Remove(author);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        private async Task<IActionResult> MapUserToAdmin(Admin admin)
        {
            var user = await GetCurrentUserAsync();
            user.Admin = admin;
            var user_res = await _userManager.UpdateAsync(user);
            if (user_res == IdentityResult.Success)
            {
                Debug.WriteLine("Admin mapped to the user.");
            }
            else
            {
                Debug.WriteLine("Unable to map admin to the user.");
                return BadRequest(user_res);
            }
            admin.User = user;
            admin.UserID = user.Id;
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res > 0)
                {
                    return Ok();
                }
                else
                {
                    return BadRequest(admin_res);
                }
            }
            else
            {
                return BadRequest("Unstable Admin Model.");
            }
        }

        public async Task<IActionResult> UnmapUserFromAdmin(int id)
        {
            Admin admin = await db.Admins.FindAsync(id);
            admin.User = null;
            admin.UserID = "";
            if (ModelState.IsValid)
            {
                db.Entry(admin).State = EntityState.Modified;
                var admin_res = await db.SaveChangesAsync();
                if (admin_res == 0)//No changes detected
                {
                    return BadRequest(admin_res);
                }
                else
                {
                    var user = await GetCurrentUserAsync();
                    user.Admin = null;
                    user.AdminID = null;
                    var user_res = await _userManager.UpdateAsync(user);
                    if (user_res == IdentityResult.Success)
                    {
                        Debug.WriteLine("User updated");
                        return Ok();
                    }
                    else
                    {
                        Debug.WriteLine("Unable to update the user");
                        return BadRequest(user_res);
                    }
                }
            }
            else
            {
                return BadRequest("Unstable Model.");
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}