﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HospitalProject.Controllers
{
    public class QualificationController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public QualificationController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.Qualifications.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Qualifications.ToListAsync());
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Description")] Qualification qualification)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account
            if (ModelState.IsValid)
            {
                qualification.AdminID = user.AdminID.GetValueOrDefault();
                qualification.Admin = user.Admin;
                db.Qualifications.Add(qualification);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public async Task<ActionResult> Edit(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }
            Qualification qualification = db.Qualifications.Find(id);
            if (qualification == null)
            {
                return NotFound();
            }

            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they have no account
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they have the wrong account
            }
            return View(qualification);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind("QualificationID, AdminID, Description")] Qualification qualification)
        {
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID != qualification.AdminID)
            {
                return Forbid();//forbid because they have the wrong account
            }

            var webRoot = _env.WebRootPath;

            if (ModelState.IsValid)
            {
                db.Entry(qualification).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(qualification);
        }

        public async Task<ActionResult> Delete(int? id, int? adminid)
        {
            if (id == null || adminid == null)
            {
                return new StatusCodeResult(400);
            }

            Qualification qualification = await db.Qualifications.SingleOrDefaultAsync(q => q.QualificationID == id);
            if (qualification == null)
            {
                return NotFound();
            }
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); // forbid because they haven't logged in
            if (user.AdminID != adminid)
            {
                return Forbid(); //forbid because they are are the wrong person
            }
            return View(qualification);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id, int adminid)
        {
            Qualification qualification = await db.Qualifications.FindAsync(id);
            // Referenced https://stackoverflow.com/questions/23090459/ienumerable-where-and-tolist-what-do-they-really-do
            // Get all JobQualifications that have this QualificationID
            IEnumerable<JobQualification> jqToDelete = db.JobQualifications.ToList().Where(jq => jq.QualificationID.Equals(qualification.QualificationID));

            var user = await GetCurrentUserAsync();
            if (user.AdminID != adminid)
            {
                return Forbid();
            }

            db.JobQualifications.RemoveRange(jqToDelete);
            db.Qualifications.Remove(qualification);
            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }
    }
}