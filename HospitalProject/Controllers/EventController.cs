﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HospitalProject.Controllers
{
    public class EventController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public EventController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.Events.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.Events.ToListAsync());
            }
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Name, Description")] Event hospEvent, 
            string eventStartDate, string eventStartTime, string eventEndDate, string eventEndTime)
        {
            string startDate = eventStartDate + " " + eventStartTime;
            string endDate = eventEndDate + " " + eventEndTime;
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account
            if (ModelState.IsValid)
            {
                hospEvent.AdminID = user.AdminID.GetValueOrDefault();
                hospEvent.Admin = user.Admin;
                hospEvent.StartTime = DateTime.Parse(startDate);
                hospEvent.EndTime = DateTime.Parse(endDate);
                db.Events.Add(hospEvent);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
}