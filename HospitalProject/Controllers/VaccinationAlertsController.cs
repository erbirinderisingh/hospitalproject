﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Data;
using HospitalProject.Models;

namespace HospitalProject.Controllers
{
    public class VaccinationAlertsController : Controller
    {
        private readonly HospitalDbContext _context;

        public VaccinationAlertsController(HospitalDbContext context)
        {
            _context = context;
        }

        // GET: VaccinationAlerts
        public async Task<IActionResult> Index()
        {
            var hospitalDbContext = _context.Vaccination_Alerts.Include(v => v.Admin);
            return View(await hospitalDbContext.ToListAsync());
        }

        // GET: VaccinationAlerts/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vaccinationAlerts = await _context.Vaccination_Alerts
                .Include(v => v.Admin)
                .SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            if (vaccinationAlerts == null)
            {
                return NotFound();
            }

            return View(vaccinationAlerts);
        }

        // GET: VaccinationAlerts/Create
        public IActionResult Create()
        {
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName");
            return View();
        }

        // POST: VaccinationAlerts/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("VaccinationAlertsid,Name,Address,Age,VaccinationNumber,AdminID")] VaccinationAlerts vaccinationAlerts)
        {
            if (ModelState.IsValid)
            {
                _context.Add(vaccinationAlerts);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName", vaccinationAlerts.AdminID);
            return View(vaccinationAlerts);
        }

        // GET: VaccinationAlerts/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vaccinationAlerts = await _context.Vaccination_Alerts.SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            if (vaccinationAlerts == null)
            {
                return NotFound();
            }
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName", vaccinationAlerts.AdminID);
            return View(vaccinationAlerts);
        }

        // POST: VaccinationAlerts/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("VaccinationAlertsid,Name,Address,Age,VaccinationNumber,AdminID")] VaccinationAlerts vaccinationAlerts)
        {
            if (id != vaccinationAlerts.VaccinationAlertsid)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(vaccinationAlerts);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!VaccinationAlertsExists(vaccinationAlerts.VaccinationAlertsid))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["AdminID"] = new SelectList(_context.Admins, "AdminID", "FirstName", vaccinationAlerts.AdminID);
            return View(vaccinationAlerts);
        }

        // GET: VaccinationAlerts/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var vaccinationAlerts = await _context.Vaccination_Alerts
                .Include(v => v.Admin)
                .SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            if (vaccinationAlerts == null)
            {
                return NotFound();
            }

            return View(vaccinationAlerts);
        }

        // POST: VaccinationAlerts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var vaccinationAlerts = await _context.Vaccination_Alerts.SingleOrDefaultAsync(m => m.VaccinationAlertsid == id);
            _context.Vaccination_Alerts.Remove(vaccinationAlerts);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VaccinationAlertsExists(int id)
        {
            return _context.Vaccination_Alerts.Any(e => e.VaccinationAlertsid == id);
        }
    }
}
