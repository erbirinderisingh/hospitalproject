﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using HospitalProject.Models;
using HospitalProject.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HospitalProject.Controllers
{
    public class FindSpecialistController : Controller
    {
        private readonly HospitalDbContext db;

        public FindSpecialistController(HospitalDbContext context)
        {
            db = context;
        }

        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        public ActionResult List (int pagenum)
        {
            var _specialists = db.FindSpecialists.ToList();
            int specialistcount = _specialists.Count();
            int perpage = 3;
            int maxpage = (int)Math.Ceiling((decimal)specialistcount/perpage) - 1;
            if (maxpage < 0) maxpage = 0;
            if (pagenum < 0) pagenum = 0;
            if (pagenum > maxpage) pagenum = maxpage;
            int start = perpage * pagenum;
            ViewData["pagenum"] = (int)pagenum;
            ViewData["PaginationSummary"] = "";
            if ( maxpage > 0 )
            {
                ViewData["PaginationSummary"] = (pagenum + 1).ToString() + " of " + (maxpage + 1).ToString();

            }
            List<FindSpecialist> findSpecialists = db.FindSpecialists.Skip(start).Take(perpage).ToList();
            return View(findSpecialists);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("SpecialistFName, SpecialistLName, Phone, Gender, Speciality, AvailableDay, AvailableTime")] FindSpecialist findSpecialist)
        {
            if (ModelState.IsValid)
            {
                db.FindSpecialists.Add(findSpecialist);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(findSpecialist);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind("SpecialistID,SpecialistFName,SpecialistLName,Phone,Gender,Speciality,AvailableDay,AvailableTime")] FindSpecialist findSpecialist)
        {

            if (ModelState.IsValid)
            {
                db.Entry(findSpecialist).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(findSpecialist);
        }


        public ActionResult Edit(int? id)
        {
            if( id == null)
            {
                return new StatusCodeResult(400);
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            if ( findSpecialist == null)
            {
                return NotFound();
            }
            return View(findSpecialist);
        }

        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            if (findSpecialist == null)
            {
                return NotFound();
            }
            return View(findSpecialist);
        }

        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new StatusCodeResult(400);
            }
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            if (findSpecialist == null)
            {
                return NotFound();
            }

            return View(findSpecialist);
        }

        /* This action is to delete a country from the Country Table after pressing submit button
         and return to Country/Index
        */
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            FindSpecialist findSpecialist = db.FindSpecialists.Find(id);
            db.FindSpecialists.Remove(findSpecialist);
            db.SaveChanges();
            return RedirectToAction("Index"); // To go back to Country/Index 

        }

    }
}