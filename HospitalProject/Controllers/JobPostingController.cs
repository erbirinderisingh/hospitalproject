﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using HospitalProject.Models;
using HospitalProject.Models.ViewModels;
using HospitalProject.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HospitalProject.Controllers
{
    public class JobPostingController : Controller
    {
        private readonly HospitalDbContext db;
        private readonly IHostingEnvironment _env;

        private readonly UserManager<ApplicationUser> _userManager;
        private async Task<ApplicationUser> GetCurrentUserAsync() => await _userManager.GetUserAsync(HttpContext.User);

        public JobPostingController(HospitalDbContext context, IHostingEnvironment env, UserManager<ApplicationUser> usermanager)
        {
            db = context;
            _env = env;
            _userManager = usermanager;
        }

        public async Task<ActionResult> Index()
        {
            var user = await GetCurrentUserAsync();
            if (user != null)
            {
                if (user.AdminID == null) { ViewData["UserHasAdmin"] = "False"; }
                else { ViewData["UserHasAdmin"] = user.AdminID.ToString(); }
                return View(await db.JobPostings.ToListAsync());
            }
            else
            {
                ViewData["UserHasAdmin"] = "None";
                return View(await db.JobPostings.ToListAsync());
            }
        }

        public ActionResult Create()
        {
            JobPostingEdit jobPostingEditView = new JobPostingEdit();

            jobPostingEditView.Departments = db.Departments.ToList();
            jobPostingEditView.Qualifications = db.Qualifications.ToList();

            return View(jobPostingEditView);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind("Title, Description, Deadline, IsActive, DepartmentID")] JobPosting jobposting, string deadlineDate, string deadlineTime)
        {
            string deadline = deadlineDate + " " + deadlineTime;
            var user = await GetCurrentUserAsync();
            if (user == null) return Forbid(); //forbid because they don't have an account
            if (user.AdminID == null) return Forbid(); //forbid because no admin account
            if (ModelState.IsValid)
            {
                jobposting.AdminID = user.AdminID.GetValueOrDefault();
                jobposting.Admin = user.Admin;
                jobposting.DatePosted = DateTime.Now;
                jobposting.Deadline = DateTime.Parse(deadline);
                db.JobPostings.Add(jobposting);
                db.SaveChanges();

                return RedirectToAction("Index");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
}