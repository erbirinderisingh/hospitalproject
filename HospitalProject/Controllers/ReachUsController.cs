﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using HospitalProject.Models;
using HospitalProject.Data;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;

namespace HospitalProject.Controllers
{
    public class ReachUsController : Controller
    {
        private readonly HospitalDbContext db;

        public ReachUsController(HospitalDbContext context)
        {
            db = context;
        }

        public ActionResult Index()
        {
            return View(db.Reach_Us.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }
    }
}